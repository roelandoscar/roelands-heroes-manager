import React from 'react';

import Header from 'app/views/components/Header';
import HeroesList from 'app/views/components/HeroesList';
import Controls from 'app/views/components/Controls';

import styles from './App.module.scss';

const App = () => (
  <main className={styles.main}>
    <Header />
    <Controls />
    <HeroesList />
  </main>
);

export default App;
