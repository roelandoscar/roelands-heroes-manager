import React from 'react';
import PropTypes from 'prop-types';

import Fav from 'app/views/components/Fav';
import styles from './index.module.scss';

const MetaList = ({ meta, heroName }) => (
  <ul className={styles.meta}>
    {meta.map(item => (
      <li className={styles.meta__item} key={`meta-${item.term}-${heroName}`}>
        <span className={styles.meta__term}>{item.term}</span>
        <span className={styles.meta__def}>
          {item.def || `no ${item.term}`}
        </span>
      </li>
    ))}
  </ul>
);

MetaList.propTypes = {
  meta: PropTypes.instanceOf(Array).isRequired,
  heroName: PropTypes.string.isRequired,
};

const Hero = ({ hero, toggleFav }) => {
  const meta = [
    { term: 'Age', def: hero.age },
    { term: 'Origin', def: hero.origin },
    { term: 'Superpowers', def: hero.superpowers.join(', ') },
    { term: 'Weaknesses', def: hero.weaknesses.join(', ') },
  ];
  const toggleHeroFav = () => toggleFav(hero.name);
  return (
    <div className={[styles.hero, styles['hero--open']].join(' ')}>
      <img
        src={`/assets/images/${hero.image}`}
        alt={hero.name}
        className={styles.hero__image}
      />
      <div className={styles.hero__info}>
        <h2 className={styles.hero__name}>
          {`${hero.name} `}
          <Fav active={hero.favorite} onClick={toggleHeroFav} />
        </h2>
        <MetaList meta={meta} heroName={hero.name} />
      </div>
    </div>
  );
};

export default Hero;

Hero.propTypes = {
  hero: PropTypes.instanceOf(Object).isRequired,
  toggleFav: PropTypes.func.isRequired,
};
