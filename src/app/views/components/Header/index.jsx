import React from 'react';

import styles from './index.module.scss';

const Header = () => (
  <header>
    <h1 className={styles.title}>Heroes Manager</h1>
  </header>
);

export default Header;
