import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import Fav from 'app/views/components/Fav';

import {
  toggleFavoriteFilter,
  setHeroesSort,
  setSearch,
} from 'app/ducks/controls';
import styles from './index.module.scss';

class HeroesList extends Component {
  constructor(props) {
    super(props);

    this.state = {};
    this.sortOptions = ['name', 'origin', 'age'];
  }

  render() {
    const {
      toggleFavorites,
      setSort,
      setSearchValue,
      favorites,
      sort,
      search,
    } = this.props;
    return (
      <div className={styles.controls}>
        <div className={styles.control}>
          <input
            id="search"
            value={search}
            className={styles.input}
            onChange={e => setSearchValue(e.target.value)}
          />
          <span className={styles.label}>search</span>
        </div>
        <div className={styles.control}>
          <select
            onChange={e => {
              setSort(e.target.value);
            }}
            value={sort}
            className={styles.select}
          >
            {this.sortOptions.map(option => (
              <option value={option} key={`option-${option}`}>
                {option}
              </option>
            ))}
          </select>
          <span className={styles.label}>sort</span>
        </div>
        <div className={styles.control}>
          <Fav active={favorites} onClick={toggleFavorites} />
          <span className={styles.label}>fav</span>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ controls }) => ({
  favorites: controls.favorites,
  sort: controls.sort,
  search: controls.search,
});

const mapDispatchToProps = {
  toggleFavorites: toggleFavoriteFilter,
  setSort: setHeroesSort,
  setSearchValue: setSearch,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HeroesList);

HeroesList.propTypes = {
  toggleFavorites: PropTypes.func.isRequired,
  setSort: PropTypes.func.isRequired,
  setSearchValue: PropTypes.func.isRequired,
  favorites: PropTypes.bool.isRequired,
  sort: PropTypes.string.isRequired,
  search: PropTypes.string.isRequired,
};
