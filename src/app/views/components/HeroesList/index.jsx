import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { fetchHeroes, toggleFavoriteHero } from 'app/ducks/heroes';

import Loader from 'app/views/components/Loader';
import Hero from 'app/views/components/Hero';

import styles from './index.module.scss';

class HeroesList extends Component {
  constructor(props) {
    super(props);

    this.pageBatch = 5;

    this.state = {
      pagination: 1,
    };
  }

  componentDidMount() {
    const { fetchHeroesFromApi } = this.props;
    fetchHeroesFromApi();
  }

  toggleFav = name => {
    const { toggleFavHero } = this.props;
    toggleFavHero(name);
  };

  loadMore = () => {
    const { pagination } = this.state;
    const nextPage = pagination + 1;
    this.setState({ pagination: nextPage });
  };

  renderHeroesState() {
    const {
      loadingHeroes,
      loadingHeroesError,
      fetchHeroesFromApi,
    } = this.props;
    if (loadingHeroes) {
      return <Loader />;
    }
    if (loadingHeroesError) {
      return (
        <div>
          <img
            src="https://media.giphy.com/media/X9jXRaNEOVAo8/giphy.gif"
            alt="Error!"
          />
          <button
            type="button"
            className={styles.loadmore}
            onClick={fetchHeroesFromApi}
          >
            Try again please!
          </button>
        </div>
      );
    }
    return this.renderHeroes();
  }

  renderHeroes() {
    const { heroes, showFavorites, searchValue } = this.props;
    const { pagination } = this.state;
    return (
      <ul className={styles.list}>
        {heroes.length > 0 &&
          heroes.slice(0, pagination * this.pageBatch).map(hero => (
            <li
              className={styles.list_item}
              key={`hero-${hero.name.toLowerCase()}`}
            >
              <Hero hero={hero} key={hero.name} toggleFav={this.toggleFav} />
            </li>
          ))}
        {heroes.length === 0 && showFavorites && (
          <li className={[styles.list_item, styles.nothing].join(' ')}>
            No heroes saved as favorite yet :(
          </li>
        )}
        {heroes.length === 0 && searchValue && (
          <li className={[styles.list_item, styles.nothing].join(' ')}>
            No heroes found with this search :(
          </li>
        )}
      </ul>
    );
  }

  render() {
    const { pagination } = this.state;
    const { heroes } = this.props;
    return (
      <section className={styles.heroes_list}>
        {this.renderHeroesState()}
        {heroes.length > 0 && pagination < 4 && (
          <button
            type="button"
            className={styles.loadmore}
            onClick={this.loadMore}
          >
            Load more heroes!
          </button>
        )}
      </section>
    );
  }
}

const mapStateToProps = ({ heroes, controls }) => {
  // check sorting and filtering
  let heroItems = null;
  const sortProp = controls.sort;
  if (controls.favorites) {
    heroItems = heroes.items.filter(hero => hero.favorite);
  } else heroItems = heroes.items;
  heroItems = heroItems.sort((a, b) => {
    if (sortProp !== 'age') return a[sortProp].localeCompare(b[sortProp]);
    // set 'unknown' to a high number so it drops to the end of the list.
    return (
      (Number.isInteger(parseInt(a.age, 10)) ? a.age : 100000) -
      (Number.isInteger(parseInt(b.age, 10)) ? b.age : 100000)
    );
  });

  // Search in name
  if (controls.search)
    heroItems = heroItems.filter(hero =>
      Object.values(hero).some(val => {
        let searchString = val;
        if (Array.isArray(val)) searchString = val.join(' ');
        if (val === false || val === true) searchString = '';
        return searchString
          .toLowerCase()
          .includes(controls.search.toLowerCase());
      })
    );

  return {
    heroes: heroItems,
    loadingHeroesError: heroes.fetchError,
    loadingHeroes: heroes.fetchLoading,
    showFavorites: controls.favorites,
    searchValue: controls.search,
    sort: controls.sort,
  };
};

const mapDispatchToProps = {
  fetchHeroesFromApi: fetchHeroes,
  toggleFavHero: toggleFavoriteHero,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HeroesList);

HeroesList.propTypes = {
  fetchHeroesFromApi: PropTypes.func.isRequired,
  toggleFavHero: PropTypes.func.isRequired,
  searchValue: PropTypes.string.isRequired,
  loadingHeroes: PropTypes.bool.isRequired,
  showFavorites: PropTypes.bool.isRequired,
  loadingHeroesError: PropTypes.string.isRequired,
  heroes: PropTypes.instanceOf(Array).isRequired,
};
