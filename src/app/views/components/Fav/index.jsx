import React from 'react';
import PropTypes from 'prop-types';

const Fav = ({ active, onClick }) => (
  <svg
    width="28px"
    height="28px"
    viewBox="0 0 29 27"
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
    xmlnsXlink="http://www.w3.org/1999/xlink"
    onClick={onClick}
  >
    <g id="group" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <path
        d="M14.0001766,0.724697475 L10.2721439,9.6454912 L9.83893873,9.68312327 L0.721056324,10.4751839 C0.720593467,10.4752242 0.716864794,10.4868285 0.722614509,10.4918594 L7.96325763,16.8353524 L5.7944594,26.2676345 C5.79232742,26.2769045 5.79336286,26.2776654 5.79071515,26.2792765 L14.0008953,21.2724819 L14.3750552,21.5006549 L22.2123117,26.2800321 C22.2088593,26.2779172 22.2095707,26.2773949 22.2073233,26.2676 L20.0385313,16.8353449 L20.3615828,16.5523301 L27.2780898,10.4930055 C27.2829506,10.4887525 27.2790422,10.476615 27.2794837,10.4766535 L17.7281798,9.64694197 L14.0001766,0.724697475 Z"
        stroke="#FFFFFF"
        strokeWidth="2"
        fill={active ? '#FFFFFF' : 'none'}
      />
    </g>
  </svg>
);

export default Fav;

Fav.propTypes = {
  active: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired,
};
