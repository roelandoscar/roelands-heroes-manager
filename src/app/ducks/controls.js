import { createAction, handleActions } from 'redux-actions';

// Types
const TOGGLE_FAVORITE_FILTER = 'TOGGLE_FAVORITE_FILTER';
const SET_SORT = 'SET_SORT';
const SET_SEARCH = 'SET_SEARCH';

// Actions
export const toggleFavoriteFilter = createAction(TOGGLE_FAVORITE_FILTER);
export const setSort = createAction(SET_SORT, sort => sort);
export const setSearch = createAction(SET_SEARCH, search => search);

// State
const initialState = {
  favorites: false,
  sort: 'name',
  search: '',
};

// Reducers
export default handleActions(
  {
    TOGGLE_FAVORITE_FILTER: state => ({
      ...state,
      favorites: !state.favorites,
    }),
    SET_SORT: (state, action) => ({
      ...state,
      sort: action.payload,
    }),
    SET_SEARCH: (state, action) => ({
      ...state,
      search: action.payload,
    }),
  },
  initialState
);

// Selectors
export const setHeroesSort = sort => dispatch => {
  dispatch(setSort(sort));
};
