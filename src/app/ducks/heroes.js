import { createAction, handleActions } from 'redux-actions';
import * as heroesManagerApi from 'app/utils/api';

// Types
const FETCH_HEROES_BEGIN = 'FETCH_HEROES_BEGIN';
const FETCH_HEROES_SUCCES = 'FETCH_HEROES_SUCCES';
const FETCH_HEROES_FAILURE = 'FETCH_HEROES_FAILURE';
const TOGGLE_FAV_HERO = 'TOGGLE_FAV_HERO';

// Actions
export const getHeroesBegin = createAction(FETCH_HEROES_BEGIN);
export const getHeroesSucces = createAction(FETCH_HEROES_SUCCES, data => data);
export const getHeroesFailure = createAction(FETCH_HEROES_FAILURE);
export const toggleFavHero = createAction(TOGGLE_FAV_HERO);

// State
const initialState = {
  items: [],
  fetchLoading: false,
  fetchError: '',
};

// Reducers
export default handleActions(
  {
    FETCH_HEROES_BEGIN: state => ({ ...state, fetchLoading: true }),
    FETCH_HEROES_SUCCES: (state, action) => ({
      ...state,
      items: action.payload,
      fetchLoading: false,
      fetchError: '',
    }),
    FETCH_HEROES_FAILURE: (state, payload) => ({
      ...state,
      fetchLoading: false,
      fetchError: payload.error,
    }),
    TOGGLE_FAV_HERO: (state, action) => {
      const updatedItems = state.items.map(item => {
        if (action.payload !== item.name) return item;
        return {
          ...item,
          favorite: !item.favorite,
        };
      });
      return {
        ...state,
        items: updatedItems,
      };
    },
  },
  initialState
);

// Selectors
export const fetchHeroes = () => dispatch => {
  dispatch(getHeroesBegin());
  try {
    heroesManagerApi.fetch((error, data) => {
      if (error instanceof Error) dispatch(getHeroesFailure(error));
      else {
        // add favorites property
        dispatch(
          getHeroesSucces(
            data.map(item => ({
              ...item,
              favorite: false,
            }))
          )
        );
      }
    });
  } catch (e) {
    // console.log('ERROR', e);
  }
};

export const toggleFavoriteHero = heroName => dispatch => {
  dispatch(toggleFavHero(heroName));
};
